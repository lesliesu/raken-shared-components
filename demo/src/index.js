import React from "react";
import { render } from "react-dom";
import { Row, Col } from "antd";
import "antd/dist/antd.css";
import "./styles.css";

import List from "../../src/components/List";
import A from "../../src/components/A";
import IconUser from "../../src/components/IconUser";
import Dropdown from "../../src/components/dropdown/Dropdown";

const data = [
  {
    companyName: "company name",
    links: ["dashboard", "project", "team", "company"]
  },
  {
    companyName: "company name",
    links: ["dashboard", "project", "team", "company"]
  },
  {
    companyName: "company name",
    links: ["dashboard", "project", "team", "company"]
  },
  {
    companyName: "company name",
    links: ["dashboard", "project", "team", "company"]
  }
];

const dropDownData = [
  { text: "my profile", count: 10 },
  { text: "my projects", count: 5 },
  { text: "tasks" }
];

class Demo extends React.Component {
  render() {
    return (
      <div>
        <h1>Raken Shared Standalone Components</h1>
        <h2>List</h2>
        <List
          dataSource={data}
          renderItem={item => (
            <List.Item>
              <Row className={"w-100"}>
                <Col span={6}>
                  <A>{item.companyName}</A>
                </Col>
                <Col span={6}>
                  <A active>Content</A>
                </Col>
              </Row>
            </List.Item>
          )}
        />

        <h2>Customer Icons</h2>
        <IconUser size={50} />

        <h2>Dropdown</h2>
        <Dropdown items={dropDownData} visible />
      </div>
    );
  }
}

render(<Demo />, document.querySelector("#demo"));
