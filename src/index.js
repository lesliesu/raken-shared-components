
import List from "./components/List";
import A from "./components/A";
import IconUser from "./components/IconUser";
import Icon from "./components/Icon";
import Badge from "./components/Badge";
import Dropdown from "./components/dropdown/Dropdown";
import MenuItem from "./components/dropdown/MenuItem";

export default {
  IconUser, List, Icon, Badge, A, Dropdown, MenuItem
};