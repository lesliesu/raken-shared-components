export const BLACK = '#4A4A4A';
export const ORANGE = '#F26620';
export const LIGHT_GREY = '#C9C9C9';
export const GREY = '#ECECEC';
export const DARK_GREY= '#D8D8D8';