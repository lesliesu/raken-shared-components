import { List as ListAntd } from 'antd';
import styled from 'styled-components';

import * as colors from '../constants/colors';

const Item = styled(ListAntd.Item)`
  background: #F3F3F3;
  color: ${colors.BLACK};
  margin: 10px 0;
  border-radius: 0;
  padding: 30px 24px;
  cursor: pointer;

  &:hover {
    box-shadow: 0 2px 4px 0 rgba(0,0,0,0.5);
  }
`;

export default class List extends ListAntd {
  static Item = Item;
}

