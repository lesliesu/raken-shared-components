import React from "react";
import PropTypes from "prop-types";
import * as colors from "../constants/colors";

// This component has been added for future use only.
// usage: <Icon icon={ICONS.USER} />
const Icon = props => {
  const styles = {
    svg: {
      display: "inline-block",
      verticalAlign: "middle"
    },
    path: {
      fill: props.color || colors.LIGHT_GREY
    }
  };

  return (
    <svg
      style={styles.svg}
      width={`${props.size}px`}
      height={`${props.size}px`}
      viewBox="0 0 50 50"
    >
      <path style={styles.path} d={props.icon} />
    </svg>
  );
};

Icon.propTypes = {
  icon: PropTypes.string.isRequired,
  size: PropTypes.number,
  color: PropTypes.string,
};

Icon.defaultProps = {
  size: 32
};

export default Icon;
