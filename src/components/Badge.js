import styled from "styled-components";
import * as colors from "../constants/colors";

const Badge = styled.div`
  width: 20px;
  height: 20px;
  line-height: 20px;
  border-radius: 50%;
  font-size: 12px;
  color: #FFFFFF;
  background-color: ${colors.ORANGE};
  display: inline-block;
  text-align: center;
`;

export default Badge;