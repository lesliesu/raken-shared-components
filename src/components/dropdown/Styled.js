import styled from "styled-components";

import * as colors from "../../constants/colors";

export const StyledDropDownWrapper = styled.div`
  box-shadow: 0 2px 30px 0 rgba(0, 0, 0, 0.3);
  position: relative;
  z-index: 100;
  width: 200px;
  border-radius: 9px;
  border: 1px solid ${colors.GREY};
  letter-spacing: -0.5px;
  top: 19px;
  &:after,
  &:before {
    bottom: 100%;
    border: solid transparent;
    content: " ";
    height: 0;
    width: 0;
    position: absolute;
    pointer-events: none;
  }

  &:after {
    border-color: rgba(255, 255, 255, 0);
    border-bottom-color: #ffffff;
    border-width: 16px;
    left: 75%;
    margin-left: -16px;
  }

  &:before {
    border-bottom-color: ${colors.GREY};
    border-width: 17px;
    left: 75%;
    margin-left: -17px;
  }
`;

export const StyledSpanText = styled.div`
  margin-left: 24px;
  width: calc(100% - 88px);
  display: inline-block;
`;

export const StyledSpanCount = styled.div`
  display: inline-block;
  width: 40px;
  text-align: right;
`;

export const StyledMenuItemContainer = styled.div`
  background-color: #ffffff;
  cursor: pointer;
  &:hover {
    background-color: ${colors.DARK_GREY};
  }
`;

export const StyledMenuItemWrapper = styled.div`
  background-color: #ffffff;
  text-transform: uppercase;
  padding: 18px 0;
  &:hover {
    background-color: ${colors.DARK_GREY};
  }
`;

export const StyledHLine = styled.div`
  border-bottom: solid 1px ${colors.DARK_GREY};
  margin: 0 24px;
  height: 1px;
  z-index: -100;
`;