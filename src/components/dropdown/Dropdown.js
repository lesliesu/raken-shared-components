import React from "react";
import PropTypes from "prop-types";

import MenuItem from "./MenuItem";
import {
  StyledDropDownWrapper,
  StyledMenuItemContainer,
  StyledHLine
} from "./Styled";

const Dropdown = props => {
  const { onClick, items, visible } = props;
  const renderItems = () =>
    items.map((data, idx) => {
      const theProps = { data, onClick };
      return (
        <StyledMenuItemContainer key={idx}>
          <MenuItem {...theProps} />
          {idx < items.length - 1 && <StyledHLine />}
        </StyledMenuItemContainer>
      );
    });
  return visible && <StyledDropDownWrapper>{renderItems()}</StyledDropDownWrapper>;
};

Dropdown.propTypes = {
  onClick: PropTypes.func,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.string.isRequired,
      count: PropTypes.number
    })
  ).isRequired,
  visible: PropTypes.bool.isRequired,
};

export default Dropdown;
