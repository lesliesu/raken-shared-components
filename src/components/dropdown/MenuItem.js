import React from "react";
import PropTypes from "prop-types";

import {
  StyledMenuItemWrapper,
  StyledSpanText,
  StyledSpanCount
} from "./Styled";
import Badge from "../Badge";

const MenuItem = props => {
  return (
    <StyledMenuItemWrapper
      onClick={e => (props.onClick ? props.onClick(props.data, e) : null)}
    >
      <StyledSpanText>{props.data.text}</StyledSpanText>
      <StyledSpanCount>
        {props.data.count > 0 && <Badge>{props.data.count}</Badge>}
      </StyledSpanCount>
    </StyledMenuItemWrapper>
  );
};

MenuItem.propTypes = {
  onClick: PropTypes.func,
  data: PropTypes.shape({
    text: PropTypes.string.isRequired,
    count: PropTypes.number
  }).isRequired
};

export default MenuItem;
