import styled from "styled-components";

import * as colors from "../constants/colors";

export default styled.a.attrs({
  target: "_blank"
})`
  color: ${props =>
    props.active ? colors.ORANGE : colors.BLACK} !important;
  text-decoration: none;
  cursor: pointer;

  &:hover {
    color: ${colors.ORANGE} !important;
  }
`;
