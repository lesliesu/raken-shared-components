module.exports = {
  type: 'react-component',
  npm: {
    esModules: true,
    umd: {
      global: 'RakenSharedComponents',
      externals: {
        react: 'React'
      }
    }
  }
}
